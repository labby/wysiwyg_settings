<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2022-2022 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php
/*
$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}
*/

if(isset ($_GET['tool'])) {
	$toolname = $_GET['tool'];
} else {
	die('[1]');
}

// get instance of own class
$oWS = wysiwyg_settings::getInstance();

if(isset ($_GET['tool']) && (empty($_POST)) )  
{
	$oWS->display_settings();
}

if(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) 
{
	$oWS->show_info();
}

if(isset ($_POST['save_settings']) ) 
{
	$oWS->save_settings();
}
