<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2022-2022 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory	= 'wysiwyg_settings';
$module_name		= 'Wysiwyg Settings';
$module_function	= 'tool';
$module_version		= '1.0.0';
$module_platform	= '5.x';
$module_delete		=  true;
$module_author		= 'LEPTON team';
$module_home		= 'http://www.lepton-cms.com';
$module_guid		= '5fcb6f56-29d0-40e9-ba6e-41e2bcc11dd6';
$module_license		= '<a href="https://www.gnu.org/licenses/gpl-3.0.en.html" target="_blank">GNU General Public License 3</a>';
$module_license_terms	= '-';
$module_description	= 'Admintool to handle and manage settings for wysiwyg editors.';
