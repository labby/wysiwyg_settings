<?php

/**
 * @module          Wysiwyg Settings
 * @author          LEPTON team
 * @copyright       2022-2022 LEPTON team
 * @link            https://lepton-cms.org
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

class wysiwyg_settings_defaults extends LEPTON_abstract implements LEPTON_wysiwyg_interface
{
    public $default_width   = "100";
    public $default_height  = "300";
    public $default_skin    = "none";
    public $default_toolbar = "none";
    // compatible to wysiwyg_settings
    public $toolbar         = "none";

    public $skins           = ["none" => ""];
    public $toolbars        = [ "none" => ""];
    
    public static $instance;

    public function initialize()
    {
        // Nothing to do here now
    }

    // interfaces
    //  [1]
    public function getHeight()
    {
        return $this->default_height;
    }

    //  [2]
    public function getWidth()
    {
        return $this->default_width;
    }

    //  [3]
    public function getToolbar()
    {
        return $this->toolbars[$this->default_toolbar];
    }

    //  [4]
    public function getSkin()
    {
        return $this->default_skin ?? "";
    }

}	
